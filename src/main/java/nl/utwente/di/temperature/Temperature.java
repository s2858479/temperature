package nl.utwente.di.temperature;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Temperature extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit";
        try {
            double input = Double.parseDouble(request.getParameter("temp"));
            out.println(docType +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>Celsius: " +
                    input + "\n" +
                    "  <P>Fahrenheit: " +
                    toFahrenheit(input) +
                    "</BODY></HTML>");
        } catch (NumberFormatException e) {
            out.println(docType +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>That was not a correct value" +
                    "</BODY></HTML>");
        }
    }

    public static double toFahrenheit(double celsius) {
        return celsius / (double)5 * (double)9 + (double)32;
    }
}
