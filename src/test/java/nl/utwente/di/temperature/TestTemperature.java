package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTemperature {
    @Test
    public void testTemp() throws Exception {
        Assertions.assertEquals(32, Temperature.toFahrenheit(0));
        Assertions.assertEquals(41, Temperature.toFahrenheit(5));
        Assertions.assertEquals(212, Temperature.toFahrenheit(100));
    }
}
